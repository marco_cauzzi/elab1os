#!/bin/bash
nomeFile=aule.txt
function prenota {
	echo Inserisci l\'aula
	read aula
	echo Inserisci il giorno
	read giorno
	echo Inserisci l\'ora
	read ora
	
	if [ "$ora" -lt "8" ]  || [ "$ora" -gt "17" ]; #Check if orario is valid
	then
		echo Orario Errato
		return
	fi
	
	echo Inserisci il tuo nome
	read nome
	#inserire se <10 aggiungere uno 0
	if [ "$ora" -lt "10" ]
	then 
		ora="0$ora"
	fi
	grep -q "$aula;$giorno;$ora" $nomeFile
	if grep -q "$aula;$giorno;$ora" $nomeFile;
	then
		echo Aula occupata
		return -1
	else
		echo "$aula;$giorno;$ora;$nome" >> "$nomeFile"
		return 0
	fi
}

function elimina {
	echo Inserisci l\'aula
	read aula
	echo Inserisci il giorno
	read giorno
	echo Inserisci l\'ora
	read ora
	
	if grep -q "$aula;$giorno;$ora" $nomeFile;
	then
		sed -i  /$(grep "$aula;$giorno;$ora" $nomeFile)/d $nomeFile
		return 0
	else
		echo Prenotazione non esistente
		return -1
	fi
}

function showAula {
	echo Inserisci l\'aula
	read aula
	
	filt=$(grep ^$aula $nomeFile)
	filt=${filt//"$aula"/""} #removes the aula field from the strings
	
	tempFile=$(mktemp)
	echo "$filt">>$tempFile
	filt=$(sort -t ";" -n $tempFile)
	
	echo Giorno Ora Utente
	for arg in "${filt[@]}"
	do
		arg=${arg//";"/" "} 
		echo "$arg"
	done
	filt=[]
} 

function numRes {
	tempFile=$(mktemp)
	cp $nomeFile $tempFile
	
	while [ -s $tempFile ]
	do
		aula=$(head -n 1 $tempFile |cut -d";" -f1) #gets the aula's name form the first line
		numApp=$(grep -c ^$aula $tempFile)
		countList+="$aula;$numApp "  
		sed -i  /^$aula/d $tempFile
	done

	tempFile=$(mktemp)
	countList=${countList//" "/$'\n'} 
    echo "$countList">>$tempFile
    arr=$(sort -t ";" -k 2 $tempFile) 
    echo $arr|tr " " $'\n'|tr ";" " "
}
scelta=1
while [ $scelta -ne 5 ] 
	do
	echo 1. Prenota
	echo 2. Elimina Prenotazione
	echo 3. Mostra aula
	echo 4. Prenotazioni per aula
	echo 5. Esci
	read scelta
	case $scelta in
	"1")prenota
	;;
	"2")elimina
	;;
	"3")showAula
	;;
	"4")numRes
	;;
	esac
done
